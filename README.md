# Embedded Systems Courses

__This repo will used to centralise pertinent courses for embedded devices.__

_Rules : if you want to add a new link to the repo, be sure to respect the category and the layout otherwise your pull request will not be merged_



### Youtube : 
[Jacques Gangloff (french)](https://www.youtube.com/watch?v=Dc2rBBhYZbI)

### Books : 
[First step with embedded systems](https://www.phaedsys.com/principals/bytecraft/bytecraftdata/bcfirststeps.pdf) \
[Nand2Tetris project](https://www.coursera.org/learn/nand2tetris2)

### Games : 
[TIS-100](https://store.steampowered.com/app/370360/TIS100/)